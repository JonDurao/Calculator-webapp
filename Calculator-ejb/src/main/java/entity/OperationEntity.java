package entity;

import pojos.Operation;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table (name = "operations")
@NamedQueries({
        @NamedQuery(name = "Operation.All", query = "SELECT a FROM OperationEntity a"),
        @NamedQuery(name = "Operation.OneId", query = "SELECT DISTINCT a FROM OperationEntity a WHERE a.id = :id")
})
public class OperationEntity {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    @Column (name = "idoperations")
    private Integer id;

    @Column (name = "input1")
    private Double i1;

    @Column (name = "input2")
    private Double i2;

    @Column (name = "symbol")
    private String symbol;

    @Column (name = "operations")
    private String operation;

    @Temporal(TemporalType.TIMESTAMP)
    @Column (name = "autotimestamp")
    private Date autoTimeStamp;

    public OperationEntity() {}

    public OperationEntity(Double i1, Double i2, String symbol, String operation) {
        this.i1 = i1;
        this.i2 = i2;
        this.symbol = symbol;
        this.operation = operation;
    }

    public OperationEntity(Operation pojo){
        id = pojo.getId();
        i1 = pojo.getI1();
        i2 = pojo.getI2();
        symbol = pojo.getSymbol();
        operation = pojo.getOperation();
        autoTimeStamp = pojo.getAutoTimeStamp();
    }

    public Integer getId() {return id;}

    public void setId(Integer id) {this.id = id;}

    public Double getI1() {return i1;}

    public void setI1(Double i1) {this.i1 = i1;}

    public Double getI2() {return i2;}

    public void setI2(Double i2) {this.i2 = i2;}

    public String getSymbol() {return symbol;}

    public void setSymbol(String symbol) {this.symbol = symbol;}

    public String getOperation() {return operation;}

    public void setOperation(String operation) {this.operation = operation;}

    public Date getAutoTimeStamp() {return autoTimeStamp;}

    public void setAutoTimeStamp(Date autoTimeStamp) {this.autoTimeStamp = autoTimeStamp;}

    @Override
    public String toString() {
        return "OperationEntity{" +
                "id=" + id +
                ", i1=" + i1 +
                ", i2=" + i2 +
                ", symbol='" + symbol + '\'' +
                ", operation='" + operation + '\'' +
                ", autoTimeStamp=" + autoTimeStamp +
                '}';
    }
}
