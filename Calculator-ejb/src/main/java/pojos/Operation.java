package pojos;

import entity.OperationEntity;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Operation  implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;
    private Double i1;
    private Double i2;
    private String symbol;
    private String operation;
    private Date autoTimeStamp;

    public Operation() {}

    public Operation(Double i1, Double i2, String symbol, String operation){
        this.i1 = i1;
        this.i2 = i2;
        this.symbol = symbol;
        this.operation = operation;
    }

    public Operation(Integer id, Double i1, Double i2, String symbol, String operation, Date autoTimeStamp) {
        this.id = id;
        this.i1 = i1;
        this.i2 = i2;
        this.symbol = symbol;
        this.operation = operation;
        this.autoTimeStamp = autoTimeStamp;
    }

    public Operation(OperationEntity entity){
        id = entity.getId();
        i1 = entity.getI1();
        i2 = entity.getI2();
        symbol = entity.getSymbol();
        operation = entity.getOperation();
        autoTimeStamp = entity.getAutoTimeStamp();
    }

    public Integer getId() {return id;}

    public void setId(Integer id) {this.id = id;}

    public Double getI1() {return i1;}

    public void setI1(Double i1) {this.i1 = i1;}

    public Double getI2() {return i2;}

    public void setI2(Double i2) {this.i2 = i2;}

    public String getSymbol() {return symbol;}

    public void setSymbol(String symbol) {this.symbol = symbol;}

    public String getOperation() {return operation;}

    public void setOperation(String operation) {this.operation = operation;}

    public Date getAutoTimeStamp() {return autoTimeStamp;}

    public void setAutoTimeStamp(Date autoTimeStamp) {this.autoTimeStamp = autoTimeStamp;}
}
