package database;

import entity.OperationEntity;
import exceptions.DbException;
import pojos.Operation;

import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.persistence.*;
import javax.transaction.UserTransaction;
import java.util.ArrayList;
import java.util.List;

// Stateless marks the class as TRANSACTIOINAL!!!
@Stateless
public class OperationDbBean {
    //EntityManagerFactory emf = Persistence.createEntityManagerFactory("OperationPR");
    //EntityManager em = emf.createEntityManager();

    @PersistenceContext
    private EntityManager em;

    public void insertOperation(Operation pojo) throws DbException{
        try {
            em.persist(new OperationEntity(pojo));
        } catch (Exception e) {
            e.printStackTrace();
            throw new DbException();
        }
    }

    public void removeOperation(Integer id) throws DbException{
        try {
            Operation operation = findOperationById(id);

            // entity in an earlier trx, so we check if the em contains it and tif not we make it manage it by merging it
            em.remove(em.find(OperationEntity.class, operation.getId()));
        } catch (Exception e) {
            e.printStackTrace();
            throw new DbException();
        }
    }

    public void updateOperation(Operation pojo) throws DbException{
        try {
            em.merge(pojo);
        } catch (Exception e) {
            e.printStackTrace();
            throw new DbException();
        }
    }

    public List findAllOps() throws DbException {
        try{
            List<Operation> pojos = new ArrayList<>();
            List<OperationEntity> entities = em.createNamedQuery("Operation.All").getResultList();

            for (OperationEntity entity : entities){
                pojos.add(new Operation(entity));
            }

            return pojos;
        } catch (Exception e){
            e.printStackTrace();
            throw new DbException();
        }
    }

    public Operation findOperationById(Integer id) throws DbException {
        try {
            OperationEntity entity = (OperationEntity) em.createNamedQuery("Operation.OneId").setParameter("id", id).getSingleResult();
            //List<OperationEntity> entities = em.createNamedQuery("Operation.One", )
            return new Operation(entity);
        } catch (NoResultException nre){
            return null;
        } catch (Exception e){
            e.printStackTrace();
            throw new DbException();
        }
    }
}
