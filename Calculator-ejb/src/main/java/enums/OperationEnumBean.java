package enums;

import javax.faces.model.SelectItem;
import java.util.ArrayList;
import java.util.List;

public enum OperationEnumBean {
    ADD_BEAN ("+"){
        public double doOperation(double a, double b){
            return a + b;
        }},
    SUBSTRACT_BEAN ("-") {
        public double doOperation(double a, double b){
            return a - b;
        }},
    MULTIPLY_BEAN ("*"){
        public double doOperation(double a, double b){
            return a * b;
        }},
    DIVIDE_BEAN ("/"){
        public double doOperation(double a, double b){
            return a / b;
        }};

    private final String symbol;

    OperationEnumBean(String symbol){
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }

    /*Metodos combo box devolver listado SelectItem*/
    public static List getSymbols (){
        List<SelectItem> symbolsOps = new ArrayList<>();

        for (OperationEnumBean op: OperationEnumBean.values()){
            symbolsOps.add(new SelectItem(op, op.symbol));
        }
        return symbolsOps;
    }

    public abstract double doOperation (double a, double b);
}
