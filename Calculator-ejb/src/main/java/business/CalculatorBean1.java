package business;

import database.OperationDbBean;
import enums.OperationEnumBean;
import exceptions.DbException;
import pojos.*;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.inject.Named;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceUnit;
import javax.transaction.*;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class CalculatorBean1 {
    public static final String BEAN_NAME = "calculadoraBean";

    @Inject
    private OperationDbBean operationDbBean;

    public Double doOpeartionBean (OperationEnumBean operation, Double a, Double b) throws DbException {
        StringBuilder sb = new StringBuilder();
        Double result = operation.doOperation(a, b);

        sb.append(a).append(" " + operation.getSymbol() + " ").append(b).append(" = ").append(result);

        try {
            //operationDbBean.insertOperation(new OperationEntity(a, b, operation.getSymbol(), sb.toString()));
            operationDbBean.insertOperation(new Operation(a, b, operation.getSymbol(), sb.toString()));
        } catch (DbException e){
            throw e;
        }

        return operation.doOperation(a, b);
    }

    public List getAllOps() throws DbException {
        try {
            return operationDbBean.findAllOps();
        } catch (Exception e){
            throw new DbException();
        }
    }

    public void removeItems(List<Operation> listOpsBean) throws DbException{
        for (Operation pojo: listOpsBean){
            try {
                operationDbBean.removeOperation(pojo.getId());
            } catch (DbException e){
                throw e;
            }
        }
    }
}
