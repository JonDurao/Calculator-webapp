package converter;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

@FacesConverter("numberConverter")
public class ConverterTextNumber implements Converter {
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) throws ConverterException {
        try {
            return Double.valueOf(value);
        } catch (ConverterException ce){
            throw ce;
        } catch (NumberFormatException nfe) {
            throw new ConverterException(new FacesMessage("Error converter"));
        }
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) throws ConverterException {
        try{
            return String.valueOf(value);
        } catch (ConverterException ce){
            throw ce;
        }
    }
}
