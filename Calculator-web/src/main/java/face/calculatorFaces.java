package face;

import business.CalculatorBean1;
import enums.OperationEnumBean;
import exceptions.DbException;
import org.primefaces.context.RequestContext;
import pojos.Operation;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.convert.ConverterException;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SessionScoped
@ManagedBean(name=calculatorFaces.BEAN_NAME)
public class calculatorFaces implements Serializable {
    public static final String BEAN_NAME = "calculadora";
    private double numberUno, numberDos, numberResult;
    private List<Operation> selectedOperations;
    private OperationEnumBean savedOperationDrop;

    @PostConstruct
    public void init(){
        numberUno = 0.0;
        numberDos = 0.0;
        numberResult = 0.0;
        savedOperationDrop = null;
    }

    @Inject
    private CalculatorBean1 calculatorBean1;

    public void faceOperation(){
        try {
            numberResult = calculatorBean1.doOpeartionBean(savedOperationDrop, numberUno, numberDos);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Success", "Operation inserted into the DDBB"));
        } catch (DbException e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Error", "Operation NOT inserted into the DDBB"));
        }
    }

    public List getData() throws DbException {
        try {
            return calculatorBean1.getAllOps();
        } catch (Exception e){
            throw new DbException();
        }
    }

    public void removeItems(){
        try {
            calculatorBean1.removeItems(selectedOperations);
        } catch (DbException e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Error", "Operation NOT removed from the DDBB"));
        }
    }

    public void showDbMessage(){
        if (selectedOperations.size() == 0){
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Error", "No items selected"));
        } else {
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO,
                    "Success!", selectedOperations.size() + " items deleted from the DB "));
        }
    }

    public List getOperationsList() {
        List<SelectItem> symbolsOps = new ArrayList<>();

        for (OperationEnumBean op: OperationEnumBean.values()){
            symbolsOps.add(new SelectItem(op, op.getSymbol()));
        }
        return symbolsOps;
    }

    public double getNumberUno() { return numberUno; }

    public void setNumberUno(double numberUno) {
        this.numberUno = numberUno;
    }

    public double getNumberDos() { return numberDos; }

    public void setNumberDos(double numberDos) { this.numberDos = numberDos; }

    public double getNumberResult() { return numberResult; }

    public void setNumberResult(double numberResult) { this.numberResult = numberResult; }

    public OperationEnumBean getSavedOperationDrop() { return savedOperationDrop; }

    public void setSavedOperationDrop(OperationEnumBean savedOperationDrop) { this.savedOperationDrop = savedOperationDrop; }

    public List<Operation> getSelectedOperations() {return selectedOperations;}

    public void setSelectedOperations(List<Operation> selectedOperations) {
        this.selectedOperations = selectedOperations;
    }
}