package validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator("customValidator")
public class CustomValidator implements Validator{
    public CustomValidator() {}

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        if (value.toString().isEmpty()){
            FacesMessage fm = new FacesMessage("Necesitamos un numero en ambos campos");

            throw new ValidatorException(fm);
        }
    }
}
