import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.math.BigDecimal;

public class CalculatorPage extends PageObject {
    @FindBy (id = "form_botones:j_idt6")
    private WebElement input1;

    @FindBy (id = "form_botones:j_idt7")
    private WebElement input2;

    @FindBy (id = "form_botones:j_idt9")
    private WebElement plusButton;

    @FindBy (id = "form_botones:output")
    private WebElement resultText;

    public CalculatorPage(WebDriver driver) {
        super(driver);
    }

    public void changeInput(String i1, String i2){
        input1.clear();
        input2.clear();

        input1.sendKeys(i1);
        input2.sendKeys(i2);

        plusButton.click();
    }

    public String getResult(){
        return resultText.getText();
    }
}
