import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.concurrent.TimeUnit;

public class SeleniumTest extends WebDriverTest {
    @Test
    public void firefoxTest(){
        // 192.168.10.40 Shaman pruebas
            driver.get("http://localhost:8090/war/");

            CalculatorPage page = new CalculatorPage(driver);
            page.changeInput("12", "24");

            WebDriverWait wait = new WebDriverWait(driver, 10);

            WebElement element = driver.findElement(By.id("growl_container"));

            wait.until(ExpectedConditions.visibilityOf(element));

            String ee = page.getResult();

            BigDecimal expectedR = BigDecimal.valueOf(12).add(BigDecimal.valueOf(24));
            BigDecimal gottenR = BigDecimal.valueOf(Float.valueOf(ee));
            int var = expectedR.compareTo(gottenR);

            Assert.assertEquals(0, var);
            System.out.println("Hola SELENIUM");
    }
}
